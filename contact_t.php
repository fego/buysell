<script type="text/javascript">
function validate()
{
	var valid=document.form;
    var erColor="#FBFFFF";
	var iChars = "!@#$%^&*=\\\';/{}|\"<>?";
									
	if(IsEmpty(valid.name))
	{
		alert("Please enter your name");
		valid.name.style.background= erColor;
		valid.name.focus();
		return false;
	}
	else
	{
	valid.name.style.background= 'White';
	valid.name.focus();
	}
       for (var i = 0; i < document.form.name.value.length; i++) {
       if (iChars.indexOf(document.form.name.value.charAt(i)) != -1)
       {
          alert ("The name field have special characters. \nThese are not allowed.\n");
	      valid.name.focus();
          return false;
       }
    }
	if(IsEmpty(valid.email))
	{
		alert("Please enter your email-address ");
		valid.email.style.background=erColor;
		valid.email.focus();
		valid.email.select();
		return false;
	}
	else
	{
		valid.email.style.background= 'White';
		valid.email.focus();
	}
	if (valid.email.value!="")
	{
		if (valid.email.value.match(/[a-zA-Z0-9]+\@[a-zA-Z0-9]+(\.(a-zA-Z0-9]{2}|[a-zA-Z0-9]{2}))+/)==null)
		{
			alert ("Please enter a valid email-address");
	        valid.email.style.background=erColor;
			valid.email.focus();
			return false;
		}
		else
		{
			valid.email.style.background= 'White';
	        valid.email.focus();
	    }
	}
	if(IsEmpty(valid.subject))
	{
		alert("Please enter subject");
		valid.subject.style.background= erColor;
		valid.subject.focus();
		return false;
	}
	else
	{
	valid.subject.style.background= 'White';
	valid.subject.focus();
	}
       for (var i = 0; i < document.form.subject.value.length; i++) {
       if (iChars.indexOf(document.form.subject.value.charAt(i)) != -1)
       {
          alert ("The name field have special characters. \nThese are not allowed.\n");
	      valid.subject.focus();
          return false;
       }
    }
	if(IsEmpty(valid.des))
	{
		alert("Please enter message");
		valid.des.style.background= erColor;
		valid.des.focus();
		return false;
	}
	else
	{
	valid.des.style.background= 'White';
	valid.des.focus();
	}
       for (var i = 0; i < document.form.des.value.length; i++) {
       if (iChars.indexOf(document.form.des.value.charAt(i)) != -1)
       {
          alert ("The name field have special characters. \nThese are not allowed.\n");
	      valid.des.focus();
          return false;
       }
    }
}
function IsEmpty(obj)
{
	var objValue;
	objValue = obj.value.replace(/\s+$/,"");
	if(objValue.length == 0)
	{return true;} 
	else{return false;}
}
</script>
<div class="about">
  <div class="container">
    <section class="title-section">
      <h1 class="title-header">Contact Us</h1>
    </section>
  </div>
</div>
<div class="contact">
  <div class="container">
    <div class="row contact_top">
    <?php
	if($_POST['email']!='')
	{
		$name=$_POST['name'];
		$email=$_POST['email'];
		$subject=$_POST['subject'];
		$message=$_POST['des'];
		$qry="insert into feedback(Name,Email,Subject,Message) values('".$name."','".$email."','".$subject."','".$message."')";
		$res=mysql_query($qry);
		printf("<script>location.href='contact.php?active=CO&success=1'</script>");
	}
	if($_REQUEST['success']==1)
	{
		echo  '<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		Thanks for your feedback!
		</div>';	
	}
	?>
      <div class="col-md-7 contact_details">
        <h5>Mailing address:</h5>
        <div class="contact_address"> VSL Technologies Pvt Ltd,<br />
ECR, Vennangupattu,
Cheyyur Taluk,
Kanchipuram District, 
Tamilnadu, 
India - 603 304.</div>
      </div>
      <div class="col-md-5 contact_details">
        <h5>Contact us:</h5>
        <div class="contact_address"> 044- 27526020</div>
        <div class="contact_mail"> info@buyandsell.in</div>
      </div>
    </div>
    <div class="contact_bottom">
      <h3>Contact Form</h3>
      <form name="form" method="post" action="contact.php?active=CO" onSubmit="return validate();">
        <div class="contact-to">
          <input type="text" class="text" name="name" placeholder="Name" maxlength="20">
          <input type="text" class="text" name="email" placeholder="Email" style="margin-left: 10px" maxlength="50">
          <input type="text" class="text" name="subject" placeholder="Subject" style="margin-left: 10px" maxlength="150">
        </div>
        <div class="text2">
          <textarea name="des" id="des" rows="5" maxlength="750" placeholder="Message"></textarea>
        </div>
        <div> <button type="submit" name="submit" class="submit" value="Submit">Send Message</button> </div>
      </form>
    </div>
  </div>
</div>