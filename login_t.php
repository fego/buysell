<?php 
session_start();
$geterr="";
if (isset($_REQUEST['er']))
$geterr = $_REQUEST['er'];
?>
<script type="text/javascript" src="js/login.js"></script>
<script type="text/javascript">
function vdt()
{
	var valid=document.forgotpwd;						
	var ercolor="#FBFFFF";
	if(valid.sEmailID.value=="")
	{
		alert("Please enter your Email address");
		valid.sEmailID.style.background=ercolor;
		valid.sEmailID.focus();
		return false;
	}
	else
	{
	valid.sEmailID.style.background= 'White';
	valid.sEmailID.focus();
	}
	if (valid.sEmailID.value!="")
	{
		if (valid.sEmailID.value.match(/[a-zA-Z0-9]+\@[a-zA-Z0-9-]+(\.(a-zA-Z0-9]{2}|[a-zA-Z0-9]{2}))+/)==null)
		{
		    alert ("Please enter a valid Email address");
			valid.sEmailID.style.background=ercolor;
			valid.sEmailID.focus();
			return false;
	    }
		else
	    {
	       valid.sEmailID.style.background= 'White';
	       valid.sEmailID.focus();
	    }
	}
}
</script>
<div class="about">
  <div class="container">
    <section class="title-section">
      <div class="row"><h1> Login</h1></div>
    </section>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
<?php
if($_REQUEST['click']=='post')
{
	echo  '<div class="form-group">
			<div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            You have to register to post seller / buyer details.
            </div></div>';
}
if( $geterr == "f" )
{
	echo  '<div class="form-group">
			<div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            Invalid UserName or Password.
            </div></div>';
}
if($_REQUEST['activate']==1)
{
	echo  '<div class="form-group">
			<div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            Thanks for visiting us again!\n\nYour account is activated successfully! Please login to post your profile.
            </div></div>';
}
?>
<form role="form" name="login" id="login" method="post" action="checklogin.php?active=PO" onSubmit="return validate()">
<div class="form-group"><input type="text" class="form-control" name="Username" placeholder="Email Address"></div>
<div class="form-group"><input type="password" class="form-control" name="Password" placeholder="Password"></div>
<div class="row form-group">
    <div class="col-md-6"><a href="register.php?active=PO">Register an account</a></div>
    <div class="col-md-6" align="right"><input type="submit" name="submit" class="btn btn-success" value="Login"></div>
</div>
</form>
            <?php
            if( $geterr == "f" )
	 		{
			?>
            <div class="form-group text-danger"><b>Forget Password</b></div>
            <form role="form" name="forgotpwd" id="fpwd" method="post" action="forgotpwd.php?active=LO" onSubmit="return vdt();" >
            <div class="row form-group">
                <div class="col-md-10"><input type="text" class="form-control" name="sEmailID" placeholder="Email Address"></div>
                <div class="col-md-2" align="right"><button type="submit" name="submit" class="btn btn-danger">Submit</button></div>
            </div>
            </form>
            <?php 
          	}
			?>
            <p>&nbsp;</p><p>&nbsp;</p>
    </div>
  </div>
</div>