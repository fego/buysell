<div class="about">
  <div class="container">
    <section class="title-section">
      <div class="row"><h1> Register</h1></div>
    </section>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
<?php
	session_start();
?>
<script type="text/javascript">
var digits = "0123456789";
var phoneNumberDelimiters = "()- ";
var validWorldPhoneChars = phoneNumberDelimiters + "+";
var minDigitsInIPhoneNumber = 10;
var cCode = 0;
function isInteger(s)
{   var i;
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
      return true;
}
function trim(s)
{   var i;
    var returnString = "";
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        if (c != " ") returnString += c;
    }
    return returnString;
}
function stripCharsInBag(s, bag)
{   var i;
    var returnString = "";
      for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}
function checkInternationalPhone(strPhone){
var bracket=3
strPhone=trim(strPhone)
if(strPhone.indexOf("+")>1) return false
if(strPhone.indexOf("-")!=-1)bracket=bracket+1
if(strPhone.indexOf("(")!=-1 && strPhone.indexOf("(")>bracket)return false
var brchr=strPhone.indexOf("(")
if(strPhone.indexOf("(")!=-1 && strPhone.charAt(brchr+2)!=")")return false
if(strPhone.indexOf("(")==-1 && strPhone.indexOf(")")!=-1)return false
s=stripCharsInBag(strPhone,validWorldPhoneChars);
return (isInteger(s) && s.length >= minDigitsInIPhoneNumber);
}
</script>
<script type="text/JavaScript">
function validate()
{
	var valid=document.form;	
    var iChars = "!@#$%^&*=\\\';/{}|\"<>?";
	var digits = "0123456789";
	var erColor="#fdffff";
	var mail=document.form.officialmail.value;
	var splitmail=mail.split('@');
    var chkmail=splitmail[1];
	
	
		//var captcha = $('#captcha').val();
	var Phone=document.form.mobile;
   
	if(IsEmpty(valid.name))
	{
		alert("Please Enter your  Name");
		valid.name.style.background= erColor;
		valid.name.focus();
		return false;
	}
	else
	{
	valid.name.style.background= 'White';
	valid.name.focus();
	}
       for (var i = 0; i < document.form.name.value.length; i++) {
       if (iChars.indexOf(document.form.name.value.charAt(i)) != -1)
       {
          alert ("The name field have special characters. \nThese are not allowed.\n");
	      valid.name.focus();
          return false;
       }
    }
	
	if(IsEmpty(valid.officialmail))
	{
		alert("Please Enter your Email-Address ");
		valid.officialmail.style.background=erColor;
		valid.officialmail.focus();
		valid.officialmail.select();
		return false;
	}
	else
	{
	valid.officialmail.style.background= 'White';
	valid.officialmail.focus();
	}
	if (valid.officialmail.value!="")
	{
		if (valid.officialmail.value.match(/[a-zA-Z0-9]+\@[a-zA-Z0-9]+(\.(a-zA-Z0-9]{2}|[a-zA-Z0-9]{2}))+/)==null)
		{
			alert ("Please Enter a Valid Email-address");
	        valid.officialmail.style.background=erColor;
			valid.officialmail.focus();
			return false;
			}
			else
			{
			valid.officialmail.style.background= 'White';
	         valid.officialmail.focus();
	        }
			if((chkmail=="gmail.com")||(chkmail=="ymail.com")||(chkmail=="yahoo.com")||(chkmail=="hotmail.com")||(chkmail=="rocketmail.com"))
			{
			 alert("please use your official email");
			 valid.officialmail.focus();
			 return false;
			}
	}		
	if(IsEmpty(valid.pwd))
	{
		alert("Please Enter Password");
		valid.pwd.style.background=erColor;
		valid.pwd.focus();
		return false;
	}	
	else
	{
	valid.pwd.style.background= 'White';
	valid.pwd.focus();
	}	
	if(IsEmpty(valid.cpwd))
	{
		alert("Please Enter Confirm Password");
		valid.cpwd.style.background=erColor;
		valid.cpwd.focus();
		return false;
		}
		else
	      {
	      valid.cpwd.style.background= 'White';
	      valid.cpwd.focus();
	      }	
		  	
	if(valid.pwd.value != valid.cpwd.value)
	{
		alert("Your Confirm Password does not match.Please Enter Correctly!");
		valid.cpwd.style.background=erColor;
		valid.cpwd.select();
		return false;
		}
		else
		{
		valid.cpwd.style.background= 'White';
		valid.cpwd.select();		
	}
	
	if ((Phone.value==null)||(Phone.value=="")){
		alert("Please Enter your Contact Number")
		Phone.focus()
		return false
	}
	if (checkInternationalPhone(Phone.value)==false){
		alert("Please Enter a Valid Contact Number")
		Phone.value=""
		Phone.focus()
		return false
	}

}
function IsEmpty(obj)
{
	var objValue;
	objValue = obj.value.replace(/\s+$/,"");
	if(objValue.length == 0)
	{return true;} 
	else{return false;}
}
</script>
<?php
 include("db.php");
 function genPwd($length=6)
 {
   $password = '';
   $possible = '23456789bcdfghjkmnpqrstvwxyz';
   $i = 0;
   while ($i < $length) 
   {
      $password .= substr($possible, mt_rand(0, strlen($possible)-1), 1);
      $i++;
   }
   return $password;
 }
   function sendActivationCode ($strEmail,$strPersonalEmail,$strUserName)
   {
       //create activation code
       $acode=genPwd(10);
       //create url
       $Myurl="http://www.buyandsell.in/activate.php?email=$strEmail&acode=$acode";
       //echo $Myurl;
       //Build email string
 
      //mail starts here...
      $to = $strEmail; 
      $subject = "buyandsell.in - Activation Mail";
      $message ="Hello! $name,\n\nPlease click the following link to activate your buyandsell account.\n\n$Myurl\n\nOr you can copy/paste in a new browser window.\n\nThanks,\buyandsell.in Team.";      
      $from = "no-reply@buyandsell.in";
      $headers = "From:" . $from;
      //Send Email
     if($strOfficialEmail!='')
	  {
	     	$personalmail=$strOfficialEmail;
      		mail($personalmail,$subject,$message,$headers);
	  }
      mail($to,$subject,$message,$headers);
      $sMessage = "New User: $strUserName";
      mail("tsperumal@gmail.com,muthu@fego.in", "new user:buyandsell.in", $sMessage, "From:gaja@fego.in");
      //echo "I am in fn activation code";
      return $acode;
   }
			
	if($_POST['name']!='')
	{
		$strUserName=$_POST['name'];
		include ("endecrypt.php");
		$strOfficialEmail=$_POST['officialmail'];
		$strPassword=$_POST['pwd'];
		$strEncryptPass = encrypt("$strPassword", "VennanguPattu");
		$nPhone=$_POST['mobile'];
		$strAcode="";
		$strAcode = sendActivationCode ($strOfficialEmail,$strPersonalEmail,$strUserName);
		$nR = rand(10000, 99999);
		$usrQry="insert into users(Name,Phone,Email,Password,Activation_Code) Values('".$strUserName."','".$nPhone."','".$strOfficialEmail."','".$strEncryptPass."','".$strAcode."')";
		$usrRes=mysql_query($usrQry);	
		if(!$usrRes)
		{
			die("register:profile insert failed:".mysql_error());
		} 	
		if($usrRes)
		{
			printf("<script>location.href='registersuccess.php?active=PO'</script>");
		}
	}

?>
<form role="form" id="form" name="form" method="post" onsubmit="return validate();" action="register.php?active=PO">
    <div class="form-group">
    	<input type="text" class="form-control" name="name" placeholder="* Name">
    </div>
    <div class="form-group">
    	<input type="text" class="form-control" name="officialmail" placeholder="* Email Address">
    </div>
    <div class="form-group">
    	<input type="password" class="form-control" name="pwd" placeholder="* Password">
    </div>
    <div class="form-group">
    	<input type="password" class="form-control" name="cpwd" placeholder="* Confirm Password">
    </div>
    <div class="form-group">
    	<input type="text" class="form-control" name="mobile" placeholder="* Mobile No">
    </div>
    <!--<div class="form-group">
    	<input type="checkbox" value="Y" checked="checked" name="accept" id="accept"> <a href="acceptterms.php" target="_blank">Accept Terms and Conditions</a>
    </div>-->
    <div class="row form-group">
        <div class="col-md-6"><a href="login.php?active=PO">Login</a></div>
        <div class="col-md-6" align="right"><input type="submit" name="submit" class="btn btn-success" value="Register"></div>
	</div>
</form>
    </div>
  </div>
</div>