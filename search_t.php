<script type="text/javascript">
function validate()
{
	var valid=document.form;
    var erColor="#FBFFFF";	
									
	if(IsEmpty(valid.campus))
	{
		alert("Please select your campus ");
		valid.campus.style.background=erColor;
		valid.campus.focus();
		return false;
	}
	else
	{
		valid.campus.style.background= 'White';
		valid.campus.focus();
	}
	if(IsEmpty(valid.category))
	{
		alert ("Please select your category ");
		valid.category.style.background=erColor;
		valid.category.focus();
		return false;
	}
	else
	{
		valid.category.style.background= 'White'
		valid.category.focus();
	}
}
function IsEmpty(obj)
{
	var objValue;
	objValue = obj.value.replace(/\s+$/,"");
	if(objValue.length == 0)
	{return true;} 
	else{return false;}
}
</script>
<div class="about">
  <div class="container">
    <section class="title-section">
      <div class="row"><h1> Search</h1></div>
    </section>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
    <?php
    if($_REQUEST['failure']==1)
	{
		echo  '<div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            Sorry, no records found, try different search!!
            </div>';
	}
	?>
<form role="form" action="sellersearchresult.php?active=SE" method="post"  name="form" id="form" onSubmit="return validate();">
    <div class="row"> 
        <div class="col-md-4 text-info"><span class="text-danger">*</span> Search for</div>
        <div class="col-md-4 form-group"><input name="searchfor" type="radio" value="Seller" id="seller" <?php if($_REQUEST['post']=="Seller"){ echo "checked";}?> /> Seller</div>
        <div class="col-md-4 form-group"><input name="searchfor" type="radio" value="Buyer" id="buyer" <?php if($_REQUEST['post']=="Buyer"){ echo "checked";}?> /> Buyer</div>
    </div>
    <div class="row"> 
    	<div class="col-md-4 text-info"><span class="text-danger">*</span> Campus</div>
        <div class="col-md-8 form-group"><select name="campus" id="campus" class="form-control">
            <option  value="">Select</option>
            <?php
             foreach ($arrCampus as $value)
  			 {
    			 echo '<option>';
    			 echo "$value";
   				 echo '</option>';echo "\n";
			 }
			?>
        	</select></div>
    </div>
    <div class="row"> 
        <div class="col-md-4 text-info"><span class="text-danger">*</span> Category</div>
        <div class="col-md-8 form-group"><select name="category" id="category" class="form-control">
            <option  value="">Select</option>
            <?php
             foreach ($arrCategory as $value)
  			 {
    			 echo '<option>';
    			 echo "$value";
   				 echo '</option>';echo "\n";
			 }
			?>
       		</select></div>
    </div>
    <div class="row">
    	<div class="col-md-4 text-info">&nbsp;&nbsp; Price</div>
        <div class="col-md-4 form-group"><input name="pricefrom" type="text" id="pricefrom" maxlength="10" class="form-control" placeholder="From"></div>
        <div class="col-md-4 form-group"><input name="priceto" type="text" id="priceto" class="form-control" placeholder="To"></div>
   </div>
   <div class="form-group" align="right">
        <button type="submit" name="Submit" class="submit" value="Submit">Submit</button>
    </div>
</form>
    </div>
  </div>
</div>