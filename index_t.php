<div class="banner text-center">
  <div class="container">
    <div class="banner-info">
      <!--<h1>Buy & Sell products</h1>
      <p>Want to buy or sell a used product in quick time from legitimate buyer/seller? Use Buy & Sell!!!</p>-->
      <label class="page-scroll"><a class="banner-btn class scroll" href="#feature"><i class="fa fa-angle-double-down fa-4x"></i></a></label>
    </div>
  </div>
</div>
<div class="main">
<div class="content_white">
  <h2>Welcome to our website!</h2>
  <p>Want to buy or sell a used product in quick time from legitimate buyer/seller? Use Buy & Sell!!!</p>
</div>
<div class="featured_content" id="feature">
  <div class="container">
    <div class="row col-md-12  text-center">
      <div class="col-md-6 feature_grid"> <i class="fa fa-shopping-cart fa-3x"></i>
        <h3>Buy</h3>
        <p>Want to buy a product, Just search your product from your campus and find the seller and buy, that's it.</p>
        <a href="search.php?active=SE&post=Buyer" class="feature_btn">More</a> </div>
      <div class="col-md-6 feature_grid"> <i class="fa fa-motorcycle fa-3x"></i>
        <h3>Sell</h3>
        <p>Want to sell a product, Post your product and rate, the buyers from your campus will contact you.</p>
        <a href="search.php?active=SE&post=Seller" class="feature_btn">More</a> </div>
    </div>
  </div>
</div>
<div class="container">
  <div class="content_white">
    <h2>Our Team</h2>
    <p>Coming together is the beginning. Staying together is progress. Working together is success!</p>
  </div>
  <div class="testimonial">
    <div class="row testimonial_top">
							<div class="col-md-12">
                                <div class="col-md-3" align="center">
                                <img class="img-responsive img-thumbnail" src="images/team/man1.jpg" alt="raman">
                                <p>&nbsp;</p>
                                <h4>Kothanda Raman</h4>
                                <h5>Director</h5>
                                </div> 
                                <div class="col-md-2" align="center">
                                <img class="img-responsive img-thumbnail" src="images/team/man2.jpg" alt="geetha">
                                <p>&nbsp;</p>
                                <h4>Geetha. S</h4>
                                <h5>VP</h5>
                                </div> 
                                <div class="col-md-2" align="center">
                                <img class="img-responsive img-thumbnail" src="images/team/man3.jpg" alt="muthu">
                                <p>&nbsp;</p>
                                <h4>Muthukumar. S</h4>
                                <h5>Technical Team Leader</h5>
                                </div>  
                                <div class="col-md-2" align="center">
                                <img class="img-responsive img-thumbnail" src="images/team/man5.jpg" alt="gajendiran">
                                <p>&nbsp;</p>
                                <h4>Gajendiran. D</h4>
                                <h5>Software Engineer</h5>
                                </div> 
                                <div class="col-md-3" align="center">
                                <img class="img-responsive img-thumbnail" src="images/team/man6.jpg" alt="praveen">
                                <p>&nbsp;</p>
                                <h4>Praveenkumar. M</h4>
                                <h5>Pre-Sales Engineer</h5>
                                </div> 
							</div>
    </div>
  </div>
</div>
<div class="container">
    <div class="content_white">
    	<h2>...Say Hello! :)</h2>
        <p>Get in touch with Buy & Sell team</p>
    </div>
    <div class="testimonial row testimonial_top">
      <div class="col-md-7 contact_details">
       	<h5>Mailing address:</h5>
       		<div class="contact_address"> VSL Technologies Pvt Ltd,<br />
            ECR, Vennangupattu,
            Cheyyur Taluk,
            Kanchipuram District, 
            Tamilnadu, 
            India - 603 304.
            </div>
      </div>
      <div class="col-md-5 contact_details">
        <h5>Contact us:</h5>
        <div class="contact_address"> <i class="fa fa-phone"></i> 044- 27526020</div>
        <div class="contact_mail"> <i class="fa fa-envelope"></i> info@buyandsell.in</div>
      </div>
    </div>
  </div>