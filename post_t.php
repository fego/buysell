<?php
if(!isset($_SESSION['email']))
{
  printf("<script>location.href='login.php?click=post&active=PO&post=Seller'</script>");
}
?>
<script type="text/javascript" src="js/post.js"></script>
<script type="text/javascript">
function postchange(str)
{
  window.location.href="post.php?active=PO&post="+str;
}
</script>
<div class="about">
  <div class="container">
    <section class="title-section">
      <div class="row"><h1> Post Seller / Buyer Details </h1></div>
    </section>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
<?php
if($_REQUEST['post']=='Seller')
{
	if($_REQUEST['exist']==1)
	{
		echo  '<div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			Sorry! A file with same name already exists, please try a different file name!';
			echo "<br>";
			$nRand = rand(1000, 9999);
			$strNoEXT=$_REQUEST['text'];
			echo "May we suggest - $strNoEXT.$nRand.$ext";
            echo '</div>';
	}
	if($_REQUEST['lessthan']==1)
	{
		echo  '<div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            Error: Files must be either JPEG, GIF, or PNG and less than 1 MB
            </div>';
	}
	if($_REQUEST['different']==1)
	{
		echo  '<div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            Error: A problem occurred during file upload!
            </div>';	
	}
	if($_POST['title']!='')
	{
		$strCampus=$_POST['campus'];
		$strCategory=$_POST['category'];
		$strPrice=$_POST['price'];
		$strTitle=$_POST['title'];
		$strDescription=$_POST['des'];
		$filename =  basename($_FILES['photo']['name']);
		if($filename=='')
		{
			$qry="insert into seller(Email,Name,Phone,Campus,Category,Price,Title,Description) values('".$_SESSION['email']."','".$_SESSION['name']."','".$_SESSION['phone']."','".$strCampus."','".$strCategory."','".$strPrice."','".$strTitle."','".$strDescription."')";
			$res=mysql_query($qry);
			printf("<script>location.href='success.php?active=PO'</script>");
		}
		if((!empty($_FILES["photo"])) && ($_FILES['photo']['error'] == 0))
		{
			$filename =  basename($_FILES['photo']['name']);
			$ext = substr($filename, strrpos($filename, '.') + 1);
			//echo $filename; echo "<br>";
			$strNoEXT = substr($filename, 0, strpos($filename, '.'));
			//verify for file extensions - jpeg, gif, png and size less than 1mb
			if ((($ext == "GIF") && ($_FILES["photo"]["type"] == "image/gif")
				|| ($ext == "PNG") && ($_FILES["photo"]["type"] == "image/png")
				|| ($ext == "JPG") && ($_FILES["photo"]["type"] == "image/jpeg")
				|| ($ext == "gif") && ($_FILES["photo"]["type"] == "image/gif")
				|| ($ext == "jpg") && ($_FILES["photo"]["type"] == "image/jpeg")
				|| ($ext == "png") && ($_FILES["photo"]["type"] == "image/png")) && 
				($_FILES["photo"]["size"] < 1048576))
			{
				$nR = rand(10000, 99999);
				$nRfilename=$nR.$filename;
				$nRfilename=str_replace(' ','',$nRfilename);
				$newname = dirname(__FILE__).'/images/upload/'.$nRfilename; 
				//$newname = $_SESSION['profilePicturePath'].'/'.$nRfilename;
				//upload photo if directory exists
				if (!file_exists($newname)) 
				{
					if (move_uploaded_file($_FILES['photo']['tmp_name'],$newname)) 
					{
						$qry="insert into seller(Email,Name,Phone,Campus,Category,Price,Photo,Title,Description) values('".$_SESSION['email']."','".$_SESSION['name']."','".$_SESSION['phone']."','".$strCampus."','".$strCategory."','".$strPrice."','".$nRfilename."','".$strTitle."','".$strDescription."')";
						$res=mysql_query($qry);
						printf("<script>location.href='success.php?active=PO'</script>");
						 //echo "Your Company was created Successfully!"; 
						 ob_flush();
					}
					else 
					{	
						printf("<script>location.href='post.php?active=PO&post=Seller&exist=1'</script>");
					}
				}
				else
				{
					printf("<script>location.href='post.php?active=PO&post=Seller&different=1&text=$strNoEXT'</script>");
				}
			}
			else
			{
				printf("<script>location.href='post.php?active=PO&post=Seller&lessthan=1'</script>");
			}
		}
	}//photo upload ends
?>
<form action="post.php?active=PO&post=Seller" method="post" enctype="multipart/form-data" name="form" onSubmit="return sellerValidate();">
     <div class="row"> 
        <div class="col-md-4 text-info"><span class="text-danger">*</span> Are You </div>
        <div class="col-md-4 form-group"><input name="choose" type="radio" value="Seller" id="seller" <?php if($_REQUEST['post']=="Seller"){ echo "checked";}?> onclick="postchange(this.value)" /> Seller</div>
        <div class="col-md-4 form-group"><input name="choose" type="radio" value="Buyer" id="buyer" <?php if($_REQUEST['post']=="Buyer"){ echo "checked";}?> onclick="postchange(this.value)" /> Buyer</div>
    </div>
    <div class="row"> 
    	<div class="col-md-4 text-info"><span class="text-danger">*</span> Campus</div>
        <div class="col-md-8 form-group"><select name="campus" id="campus" class="form-control">
            <option  value="">Select</option>
            <?php
             foreach ($arrCampus as $value)
  			 {
    			 echo '<option>';
    			 echo "$value";
   				 echo '</option>';echo "\n";
			 }
			?>
        	</select></div>
    </div>
    <div class="row"> 
        <div class="col-md-4 text-info"><span class="text-danger">*</span> Category</div>
        <div class="col-md-8 form-group"><select name="category" id="category" class="form-control">
            <option  value="">Select</option>
            <?php
             foreach ($arrCategory as $value)
  			 {
    			 echo '<option>';
    			 echo "$value";
   				 echo '</option>';echo "\n";
			 }
			?>
       		</select></div>
    </div>
    <div class="row">
    	<div class="col-md-4 text-info"><span class="text-danger">*</span> Price</div>
        <div class="col-md-8 form-group"><input name="price" type="text" id="price" maxlength="10" class="form-control"></div>
    </div>
    <div class="row">
        <div class="col-md-4 text-info">&nbsp;&nbsp; Photo</div>
        <div class="col-md-8 form-group"><input name="photo" type="file" id="photo" class="form-control"></div>
    </div>
   <div class="row"> 
    	<div class="col-md-4 text-info"><span class="text-danger">*</span> Title</div>
        <div class="col-md-8 form-group"><input name="title" type="text" id="title" maxlength="150" class="form-control"></div>
    </div>
    <div class="row">        
        <div class="col-md-4 text-info"><span class="text-danger">*</span> Description</div>
        <div class="col-md-8 form-group"><textarea name="des" id="des" rows="5" maxlength="750" class="form-control" placeholder="maximum 750 characters only"></textarea></div>
    </div>
    <div class="form-group" align="right">
        <button type="submit" name="Submit" class="submit" value="Submit">Submit</button>
    </div>
</form>
<?php
}
if($_REQUEST['post']=='Buyer')
{
	if($_POST['title']!='')
	{
		$strCampus=$_POST['campus'];
		$strCategory=$_POST['category'];
		$strPriceFrom=$_POST['pricefrom'];
		$strPriceTo=$_POST['priceto'];
		$strTitle=$_POST['title'];
		$strDescription=$_POST['des'];
		$qry="insert into buyer(Email,Name,Phone,Campus,Category,PriceFrom,PriceTo,Title,Description) values('".$_SESSION['email']."','".$_SESSION['name']."','".$_SESSION['phone']."','".$strCampus."','".$strCategory."','".$strPriceFrom."','".$strPriceTo."','".$strTitle."','".$strDescription."')";
		$res=mysql_query($qry);
		printf("<script>location.href='success.php?active=PO'</script>");
	}
?>
<form action="post.php?active=PO&post=Buyer" method="post" name="form" onSubmit="return buyerValidate();">
     <div class="row"> 
        <div class="col-md-4 text-info"><span class="text-danger">*</span> Are You </div>
        <div class="col-md-4 form-group"><input name="choose" type="radio" value="Seller" id="seller" <?php if($_REQUEST['post']=="Seller"){ echo "checked";}?> onclick="postchange(this.value)" /> Seller</div>
        <div class="col-md-4 form-group"><input name="choose" type="radio" value="Buyer" id="buyer" <?php if($_REQUEST['post']=="Buyer"){ echo "checked";}?> onclick="postchange(this.value)" /> Buyer</div>
    </div>
    <div class="row"> 
    	<div class="col-md-4 text-info"><span class="text-danger">*</span> Campus</div>
        <div class="col-md-8 form-group"><select name="campus" id="campus" class="form-control">
            <option  value="">Select</option>
            <?php
             foreach ($arrCampus as $value)
  			 {
    			 echo '<option>';
    			 echo "$value";
   				 echo '</option>';echo "\n";
			 }
			?>
        	</select></div>
    </div>
    <div class="row">             
        <div class="col-md-4 text-info"><span class="text-danger">*</span> Category</div>
        <div class="col-md-8 form-group"><select name="category" id="category" class="form-control">
            <option  value="">Select</option>
            <?php
             foreach ($arrCategory as $value)
  			 {
    			 echo '<option>';
    			 echo "$value";
   				 echo '</option>';echo "\n";
			 }
			?>
       		</select></div>
    </div>
    <div class="row">
    	<div class="col-md-4 text-info"><span class="text-danger">*</span> Price</div>
        <div class="col-md-4 form-group"><input name="pricefrom" type="text" id="pricefrom" maxlength="10" class="form-control" placeholder="From"></div>
        <div class="col-md-4 form-group"><input name="priceto" type="text" id="priceto" class="form-control" placeholder="To"></div>
    </div>
    <div class="row">         
    	<div class="col-md-4 text-info"><span class="text-danger">*</span> Title</div>
        <div class="col-md-8 form-group"><input name="title" type="text" id="title" maxlength="150" class="form-control"></div>
   </div>
   <div class="row"> 
        <div class="col-md-4 text-info"><span class="text-danger">*</span> Description</div>
        <div class="col-md-8 form-group"><textarea name="des" id="des" rows="5" maxlength="750" class="form-control" placeholder="maximum 750 characters only"></textarea></div>
    </div>
    <div class="form-group" align="right">
        <button type="submit" name="Submit" class="submit" value="Submit">Submit</button>
    </div>
</form>
<?php
}
?>
    </div>
  </div>
</div>