function validate()
{
	var valid=document.login;
    var erColor="#FBFFFF";										
	if(IsEmpty(valid.Username))
	{
		alert("Please Enter your Username ");
		valid.Username.style.background=erColor;
		valid.Username.focus();
		return false;
	}
	else
	{
	valid.Username.style.background= 'White';
	valid.Username.focus();
	}
	if (valid.Username.value!="")
	{
		if (valid.Username.value.match(/[a-zA-Z0-9]+\@[a-zA-Z0-9-]+(\.(a-zA-Z0-9]{2}|[a-zA-Z0-9]{2}))+/)==null)
		{
		       alert ("Please Enter a Valid Username");
			valid.Username.style.background= 'light yellow';
			valid.Username.focus();
			return false;
	        }
		else
	        {
	       valid.Username.style.background= 'White';
	       valid.Username.focus();
	     }}
	if(IsEmpty(valid.Password))
	{
		alert ("Please Enter Password ");
		valid.Password.style.background=erColor;
		valid.Password.focus();
		return false;
	}
	else
	{
	valid.Password.style.background= 'White'
	valid.Password.focus();
	}
}
function IsEmpty(obj)
{
	var objValue;
	objValue = obj.value.replace(/\s+$/,"");
	if(objValue.length == 0)
	{return true;} 
	else{return false;}
}