function sellerValidate()
{
	var valid=document.form;
    var erColor="#FBFFFF";	
	var iChars = "@#$%^+=[]\\\;{}|/<>!&*()?':=+_";	
									
	if(IsEmpty(valid.campus))
	{
		alert("Please select your campus ");
		valid.campus.style.background=erColor;
		valid.campus.focus();
		return false;
	}
	else
	{
		valid.campus.style.background= 'White';
		valid.campus.focus();
	}
	if(IsEmpty(valid.category))
	{
		alert ("Please select your category ");
		valid.category.style.background=erColor;
		valid.category.focus();
		return false;
	}
	else
	{
		valid.category.style.background= 'White'
		valid.category.focus();
	}
	if(IsEmpty(valid.price))
	{
		alert ("Please enter your price");
		valid.price.style.background=erColor;
		valid.price.focus();
		return false;
	}
	else
	{
		valid.price.style.background= 'White'
		valid.price.focus();
	}
	if(valid.price.value!='')
	{
		if(isNaN(valid.price.value))
		{
			alert("Please enter numbers only");
			valid.price.style.background=erColor;
			valid.price.focus();
			return (false);
		}
    }
	else
	{
		valid.price.style.background= 'White';
		valid.price.focus();
	}
	if(IsEmpty(valid.title))
	{
		alert ("Please enter your title");
		valid.title.style.background=erColor;
		valid.title.focus();
		return false;
	}
	else
	{
		valid.title.style.background= 'White'
		valid.title.focus();
	}
	for (var i = 0; i < valid.title.value.length; i++) 
	{
		if (iChars.indexOf(valid.title.value.charAt(i)) != -1)
		{
			alert ("Sorry, You entered special characters. \nThese are not allowed.\n");
			valid.title.focus();
			return false;
		}
    }
	if(IsEmpty(valid.des))
	{
		alert ("Please enter your description");
		valid.des.style.background=erColor;
		valid.des.focus();
		return false;
	}
	else
	{
		valid.des.style.background= 'White'
		valid.des.focus();
	}
	for (var i = 0; i < valid.des.value.length; i++) 
	{
		if (iChars.indexOf(valid.des.value.charAt(i)) != -1)
		{
			alert ("Sorry, You entered special characters. \nThese are not allowed.\n");
			valid.des.focus();
			return false;
		}
    }
}
function buyerValidate()
{
	var valid=document.form;
    var erColor="#FBFFFF";	
	var iChars = "@#$%^+=[]\\\;{}|/<>!&*()?':=+_";	
									
	if(IsEmpty(valid.campus))
	{
		alert("Please select your campus ");
		valid.campus.style.background=erColor;
		valid.campus.focus();
		return false;
	}
	else
	{
		valid.campus.style.background= 'White';
		valid.campus.focus();
	}
	if(IsEmpty(valid.category))
	{
		alert ("Please select your category ");
		valid.category.style.background=erColor;
		valid.category.focus();
		return false;
	}
	else
	{
		valid.category.style.background= 'White'
		valid.category.focus();
	}
	if(IsEmpty(valid.pricefrom))
	{
		alert ("Please enter your minimum price");
		valid.pricefrom.style.background=erColor;
		valid.pricefrom.focus();
		return false;
	}
	else
	{
		valid.pricefrom.style.background= 'White'
		valid.pricefrom.focus();
	}
	if(valid.pricefrom.value!='')
	{
		if(isNaN(valid.pricefrom.value))
		{
			alert("Please enter numbers only");
			valid.pricefrom.style.background=erColor;
			valid.pricefrom.focus();
			return (false);
		}
    }
	else
	{
		valid.pricefrom.style.background= 'White';
		valid.pricefrom.focus();
	}
	if(IsEmpty(valid.priceto))
	{
		alert ("Please enter your maximum price");
		valid.priceto.style.background=erColor;
		valid.priceto.focus();
		return false;
	}
	else
	{
		valid.priceto.style.background= 'White'
		valid.priceto.focus();
	}
	if(valid.priceto.value!='')
	{
		if(isNaN(valid.priceto.value))
		{
			alert("Please enter numbers only");
			valid.priceto.style.background=erColor;
			valid.priceto.focus();
			return (false);
		}
    }
	else
	{
		valid.priceto.style.background= 'White';
		valid.priceto.focus();
	}
	if(IsEmpty(valid.title))
	{
		alert ("Please enter your title");
		valid.title.style.background=erColor;
		valid.title.focus();
		return false;
	}
	else
	{
		valid.title.style.background= 'White'
		valid.title.focus();
	}
	for (var i = 0; i < valid.title.value.length; i++) 
	{
		if (iChars.indexOf(valid.title.value.charAt(i)) != -1)
		{
			alert ("Sorry, You entered special characters. \nThese are not allowed.\n");
			valid.title.focus();
			return false;
		}
    }
	if(IsEmpty(valid.des))
	{
		alert ("Please enter your description");
		valid.des.style.background=erColor;
		valid.des.focus();
		return false;
	}
	else
	{
		valid.des.style.background= 'White'
		valid.des.focus();
	}
	for (var i = 0; i < valid.des.value.length; i++) 
	{
		if (iChars.indexOf(valid.des.value.charAt(i)) != -1)
		{
			alert ("Sorry, You entered special characters. \nThese are not allowed.\n");
			valid.des.focus();
			return false;
		}
    }
}
function IsEmpty(obj)
{
	var objValue;
	objValue = obj.value.replace(/\s+$/,"");
	if(objValue.length == 0)
	{return true;} 
	else{return false;}
}