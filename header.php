<!DOCTYPE HTML>
<html>
<head>
<title>Buy and Sell</title>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="shortcut icon" href="images/favicon.png">
<script src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.js"></script> 
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
		});
	});
</script>
<link href="css/style.css" rel='stylesheet' type='text/css' />
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="source/jquery.fancybox.css?v=2.1.4" media="screen" />
	<script type="text/javascript" src="source/jquery.fancybox.js?v=2.1.4"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
		jQuery('.fancybox').fancybox();
		});
	</script>
</head>
<body>
<div class="header">
  <div class="container">
    <div class="logo"> <a href="index.php"><img src="images/logo.png" alt="buyandsell"></a> </div>
    <div class="menu"> <a class="toggleMenu" href="#"><img src="images/nav_icon.png" alt="" /> </a>
<?php
 session_start();
 include("db.php");
 require_once("common_inc.php");
 if((!isset($_REQUEST['active']))||($_REQUEST['active']=="HO"))
   {
   $class1="active";
   }
   if($_REQUEST['active']=="AB")
   {
   $class2="active";
   }
   if($_REQUEST['active']=="PO")
   {
   $class3="active";
   }
   if($_REQUEST['active']=="SE")
   {
   $class4="active";
   }
   if($_REQUEST['active']=="CO")
   {
   $class5="active";
   }
   if($_REQUEST['active']=="MY")
   {
   $class6="active";
   }
?>
      <ul class="nav" id="nav">
        <li class="<?php echo $class1; ?>"><a href="index.php?active=HO">Home</a></li>
        <!--<li class="<?php //echo $class2; ?>"><a href="about.php?active=AB">About</a></li>-->
        <li class="<?php echo $class3; ?>"><a href="post.php?active=PO&post=Seller">Post</a></li>
        <li class="<?php echo $class4; ?>"><a href="search.php?active=SE&post=Seller">Search</a></li>
        <?php
		if(!isset($_SESSION['email']))
		{
		?>
        <li class="<?php echo $class5; ?>"><a href="contact.php?active=CO">Contact</a></li>
        <?php
		}
		if(isset($_SESSION['email']))
		{
		?>
        <li class="<?php echo $class6; ?>"><a href="mypost.php?active=MY&post=Seller">My Post</a></li>
        <li><a href="logout.php?active=PO">Logout</a></li>
        <?php
		}
		?>
        <div class="clear"></div>
      </ul>
      <script type="text/javascript" src="js/responsive-nav.js"></script> 
    </div>
    <div class="clearfix"> </div>
  </div>
</div>